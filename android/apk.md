## Alternatives / Replacements

[F-Droid](https://f-droid.org/)\
[⬇](https://f-droid.org/F-Droid.apk)

[Aegis Authenticator](https://getaegis.app/)\
[🔷](https://play.google.com/store/apps/details?id=com.beemdevelopment.aegis/)
[🟩](https://f-droid.org/en/packages/com.beemdevelopment.aegis/)

[FairEmail](https://email.faircode.eu/)\
[🔷](https://play.google.com/store/apps/details?id=eu.faircode.email)
[🟩](https://f-droid.org/en/packages/eu.faircode.email/)

[Open Camera](https://sourceforge.net/projects/opencamera/)\
[🔷](https://play.google.com/store/apps/details?id=net.sourceforge.opencamera)
[🟩](https://f-droid.org/en/packages/net.sourceforge.opencamera/)

-----

-----

## Some root utilities

Reboot into any mode\
[🔷](https://play.google.com/store/apps/details?id=simple.reboot.com)
[🟩](https://f-droid.org/en/packages/simple.reboot.com/)

Firewall\
[🔷](https://play.google.com/store/apps/details?id=dev.ukanth.ufirewall)
[🟩](https://f-droid.org/en/packages/dev.ukanth.ufirewall/)

Clear all cache\
[🔷](https://play.google.com/store/apps/details?id=com.geekofia.cacheaway)
[🟩](https://f-droid.org/en/packages/com.geekofia.cacheaway/)

