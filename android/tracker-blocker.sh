#!/bin/sh
# Derived from https://gitlab.com/adrian.m.miller/disablegoogleanalytics/-/blob/main/service.sh
# Using https://reports.exodus-privacy.eu.org/en/trackers/?filter=apps

trackers=\
'com.applovin '\
'com.applovin.adview '\
'com.applovin.adview.AppLovinFullscreenActivity '\
'com.applovin.adview.AppLovinFullscreenThemedActivity '\
'com.applovin.creative '\
'com.applovin.creative.MaxCreativeDebuggerActivity '\
'com.applovin.creative.MaxCreativeDebuggerDisplayedAdActivity '\
'com.applovin.mediation '\
'com.applovin.mediation.hybridAds '\
'com.applovin.mediation.hybridAds.MaxHybridMRecAdActivity '\
'com.applovin.mediation.hybridAds.MaxHybridNativeActivity '\
'com.applovin.mediation.MaxDebuggerActivity '\
'com.applovin.mediation.MaxDebuggerAdUnitDetailActivity '\
'com.applovin.mediation.MaxDebuggerAdUnitsListActivity '\
'com.applovin.mediation.MaxDebuggerAdUnitsWaterfallsListActivity '\
'com.applovin.mediation.MaxDebuggerCmpNetworksListActivity '\
'com.applovin.mediation.MaxDebuggerDetailActivity '\
'com.applovin.mediation.MaxDebuggerMultiAdActivity '\
'com.applovin.mediation.MaxDebuggerTcfConsentStatusesListActivity '\
'com.applovin.mediation.MaxDebuggerTcfInfoListActivity '\
'com.applovin.mediation.MaxDebuggerTcfStringActivity '\
'com.applovin.mediation.MaxDebuggerTestLiveNetworkActivity '\
'com.applovin.mediation.MaxDebuggerTestModeNetworkActivity '\
'com.applovin.mediation.MaxDebuggerWaterfallKeyWordsActivity '\
'com.applovin.sdk '\
'com.applovin.sdk.AppLovinWebActivity '\'com.crashlytics.android.CrashlyticsInitProvider '\
'com.facebook.ads '\
'com.facebook.ads.AudienceNetworkActivity '\
'com.facebook.appevents '\
'com.facebook.CampaignTrackingReceiver '\
'com.facebook.login '\
'com.facebook.marketing '\
'com.facebook.share '\
'com.google.analytics '\
'com.google.android.apps.analytics '\
'com.google.android.gms.admob '\
'com.google.android.gms.ads '\
'com.google.android.gms.ads.AbstractAdRequestBuilder '\
'com.google.android.gms.ads.AdActivity '\
'com.google.android.gms.ads.AdapterResponseInfo '\
'com.google.android.gms.ads.AdError '\
'com.google.android.gms.ads.AdInspectorError '\
'com.google.android.gms.ads.AdListener '\
'com.google.android.gms.ads.AdLoadCallback '\
'com.google.android.gms.ads.AdLoader '\
'com.google.android.gms.ads.AdLoader.Builder '\
'com.google.android.gms.ads.AdRequest '\
'com.google.android.gms.ads.AdRequest.Builder '\
'com.google.android.gms.ads.AdSize '\
'com.google.android.gms.ads.AdValue '\
'com.google.android.gms.ads.AdView '\
'com.google.android.gms.ads.BaseAdView '\
'com.google.android.gms.ads.doubleclick '\
'com.google.android.gms.ads.FullScreenContentCallback '\
'com.google.android.gms.ads.LoadAdError '\
'com.google.android.gms.ads.mediation '\
'com.google.android.gms.ads.MediationUtils '\
'com.google.android.gms.ads.MobileAds '\
'com.google.android.gms.ads.RequestConfiguration '\
'com.google.android.gms.ads.RequestConfiguration.Builder '\
'com.google.android.gms.ads.ResponseInfo '\
'com.google.android.gms.ads.VersionInfo '\
'com.google.android.gms.ads.VideoController '\
'com.google.android.gms.ads.VideoController.VideoLifecycleCallbacks '\
'com.google.android.gms.ads.VideoOptions '\
'com.google.android.gms.ads.VideoOptions.Builder '\
'com.google.android.gms.analytics '\
'com.google.android.gms.analytics.AnalyticsJobService '\
'com.google.android.gms.analytics.AnalyticsReceiver '\
'com.google.android.gms.analytics.CampaignTrackingReceiver '\
'com.google.android.gms.analytics.CampaignTrackingService '\
'com.google.android.gms.measurement '\
'com.google.android.gms.measurement.AppMeasurementContentProvider '\
'com.google.android.gms.measurement.AppMeasurementInstallReferrerReceiver '\
'com.google.android.gms.measurement.AppMeasurementJobService '\
'com.google.android.gms.measurement.AppMeasurementReceiver '\
'com.google.android.gms.measurement.AppMeasurementService '\
'com.google.android.gms.tagmanager '\
'com.google.firebase.analytics '\
'com.google.firebase.analytics.AnalyticsKt '\
'com.google.firebase.analytics.ConsentBuilder '\
'com.google.firebase.analytics.FirebaseAnalytics '\
'com.google.firebase.analytics.FirebaseAnalytics.Event '\
'com.google.firebase.analytics.FirebaseAnalytics.Param '\
'com.google.firebase.analytics.FirebaseAnalytics.UserProperty '\
'com.google.firebase.analytics.ParametersBuilder '\
'com.google.firebase.crashlytics '\
'com.google.firebase.crashlytics.CustomKeysAndValues '\
'com.google.firebase.crashlytics.CustomKeysAndValues.Builder '\
'com.google.firebase.crashlytics.FirebaseCrashlytics '\
'com.google.firebase.crashlytics.FirebaseCrashlyticsKt '\
'com.google.firebase.crashlytics.KeyValueBuilder '\
'com.google.tagmanager '\
'com.unity3d.ads '\
'com.unity3d.services '


for apk in $(pm list packages -3 | sed 's/package://g' | sort)
do
  for i in $trackers
  do
    pm disable "$apk/$i" 2>/dev/null
  done
done

