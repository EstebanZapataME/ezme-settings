#!/bin/bash
# Should work well enough for the official Client, NekoX, Octogram, and Forkgram.
# Needs: imagemagick jpegoptim exiv2

OptiTele(){
  rm -f *-thumb.jpg
  for i in $(ls -1t | grep jpg)
  do
    printf "$i\n "
    resol=$(($(magick identify -format "%h*%w" "$i"))) # ToDo: Change this
    while [ $resol -lt 1000000 ]
    do
      nice magick convert "$i" -colorspace LAB -virtual-pixel dither -interpolate catrom -filter Mitchell -resize "2560x2560" -clamp -strip -colorspace sRGB -quality 100 "$i"
      printf " "
    done
    nice jpegoptim -m75 -f -p -P -s --all-normal "$i" | tail -c21 | head -c8 | sed 's/(//' | sed 's/)//'
    touch -c -t $(echo "$i" | head -c17 | tail -c13 | sed 's/_//').$(echo "$i" | head -c19 | tail -c2) "$i"
    printf "\n"
    Imagen=$(basename "$i" .jpg)
    nice magick convert "$i" -virtual-pixel dither -interpolate catrom -filter Box -resize "5%" -clamp -strip "$Imagen"-thumb.jpg
    nice jpegoptim -q -s --all-normal -m50 "$Imagen"-thumb.jpg
    exiv2 -k -it insert "$i"
    rm "$Imagen"-thumb.jpg
    mv "$i" $(basename "$i" g)eg
    printf "\n"
    sleep 0.1
  done
  rm -f *-thumb.jpg
}

########################################

printf "============\n"
printf "= Telegram =\n"
printf "============\n\n"

if [ -d /sdcard/NekoX/caches ]
then
    printf "Remove caches\n"
    printf "=============\n\n"
    cd /sdcard/NekoX/caches
    rm -rf -- acache
    rm -f -- *.temp
    rm -f -- *.preload
    rm -f -- *.pt
    beep
    echo "Remove HQ jpegs/webps, but keep the rest"
    rm -f -- $(ls -1 | grep -v ^-)
    touch .nomedia
    beep
fi

if [ -d "/sdcard/Android/media/org.telegram.messenger/Telegram/Telegram Images" ]; then
  cd "/sdcard/Android/media/org.telegram.messenger/Telegram/Telegram Images"
  printf "==Optimizing photographs==\n"
  OptiTele
  printf "\n=Done=\n"
elif [ -d "/sdcard/Android/media/it.octogram.android/Telegram/Telegram Images" ]; then
  cd "/sdcard/Android/media/it.octogram.android/Telegram/Telegram Images"
  printf "==Optimizing photographs==\n"
  OptiTele
  printf "\n=Done=\n"
elif [ -d "/sdcard/Android/media/org.forkgram.messenger/Telegram/Telegram Images" ]; then
  cd "/sdcard/Android/media/org.forkgram.messenger/Telegram/Telegram Images"
  printf "==Optimizing photographs==\n"
  OptiTele
  printf "\n=Done=\n"
else
  printf "No photograph folder found...\n"
fi

printf "\n\n"

if [ -d /sdcard/Pictures/Nekogram ]; then
  cd /sdcard/Pictures/Nekogram
elif [ -d /sdcard/Pictures/Telegram ]; then
  cd /sdcard/Pictures/Telegram
else
  print "No Telegram folder found...\n"
  exit 1
fi

printf "==Optimizing downloads==\n"
OptiTele
printf "\n=Done=\n"


beep && beep && beep
sleep 1
