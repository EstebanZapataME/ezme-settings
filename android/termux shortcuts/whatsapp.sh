#!/bin/bash

SeparatoR() {
echo ""
sleep 0.1
}

OMyJ() {
  rm -f *-thumb.jpg
  for i in $(ls -1t | grep jpg)
  do
    touch -c -t $(echo "$i" | head -c12 | tail -c8)0000.01 "$i"
    printf " "
    export j="$i"
    if [[ $(exiv2 -g Thumbnail "$j") == "" ]]; then
      printf "\n$i\n "
      resol=$(($(magick identify -format "%h*%w" "$i")))
      while [ $resol -lt 1000000 ]
      do
        resul=$((2*$resol))
        nice magick convert "$i" -colorspace LAB -virtual-pixel dither -interpolate catrom -filter Mitchell -resize "$resul"@ -clamp -strip -colorspace sRGB -quality 100 "$i"
        resol=$(($(magick identify -format "%h*%w" "$i")))
        printf " "
      done
      nice jpegoptim -m75 -f -p -P -s --all-normal "$i" | tail -c21 | head -c8 | sed 's/(//' | sed 's/)//'
      touch -c -t $(echo "$i" | head -c12 | tail -c8)0000.00 "$i"
      Imagen=$(basename "$j" .jpg)
      nice magick convert "$j" -virtual-pixel dither -interpolate catrom -filter Box -resize '5%' -clamp -strip "$Imagen"-thumb.jpg
      nice jpegoptim -q -s --all-normal -m50 "$Imagen"-thumb.jpg
      exiv2 -k -it insert "$j"
      rm "$Imagen"-thumb.jpg
      printf "\n\n"
    fi
done
}

# Finished establishing functions
# Now it's time to run them

if [ -d /sdcard/Whatsapp ]; then
    cd /sdcard/Whatsapp
elif [ -d /sdcard/Android/media/com.whatsapp/Whatsapp ]; then
    cd /sdcard/Android/media/com.whatsapp/Whatsapp
else
    echo "No WhatsApp folder was found :("
    exit
fi

printf "============\n"
printf "= WhatsApp =\n"
printf "============\n\n"

printf "==Removing Thumbs==\n"
rm -rfv .Shared
rm -rfv .Thumbs
rm -rfv .trash
rm -rfv .StickerThumbs
SeparatoR

printf "\n==Removing old backups==\n"
rm -rfv Databases/msgstore-*
SeparatoR

printf "\n==Removing temps==\n"
rm -rfv Media/.Links
rm -rfv Media/Statuses
SeparatoR

printf "\n==Optimizing images==\n"
cd Media/"Whatsapp Images"
OMyJ
SeparatoR

printf "\n==Optimizing sent images==\n"
cd Sent
OMyJ
SeparatoR

printf "\n=Done=\n\n"

beep
beep
beep

sleep 1
