#!/bin/bash

SeparatoR() {
echo ""
sleep 0.5
}

OhMyJ(){
for i in $(ls -1t | grep jpg)
do
    rm -f *-thumb.jpg
    printf "$i\n "
    nice jpegoptim -m81 -T1 -p -P --strip-none --all-normal "$i" | tail -c21 | head -c8 | sed 's/(//' | sed 's/)//'
    Imagen=$(basename "$i" .jpg)
    nice magick convert "$i" -virtual-pixel dither -interpolate catrom -filter Box -resize '5%' -clamp -strip "$Imagen"-thumb.jpg
    nice jpegoptim -q -s --all-normal -m50 "$Imagen"-thumb.jpg
    exiv2 -k -it insert "$i"
    rm "$Imagen"-thumb.jpg
    mv "$i" $(basename "$i" g)eg
    beep
    printf "\n\n"
done
}


if [ -d /sdcard/DCIM/Camera ]
then
    cd /sdcard/DCIM/Camera
    echo "Optimizing Google Camera photos."
    echo "================================="
    OhMyJ
    SeparatoR
fi
rm -f *-thumb.jpg

if [ -d /sdcard/DCIM/OpenCamera ]
then
    cd /sdcard/DCIM/OpenCamera
    echo "Optimizing OpenCamera photos."
    echo "=============================="
    OhMyJ
    SeparatoR
fi
rm -f *-thumb.jpg

echo "=Done="

beep
beep
beep

sleep 1

#read debug
