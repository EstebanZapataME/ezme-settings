#!/bin/bash

sudo acc -i | tee battery.txt 1>/dev/null

printf "################################\n\n"

cat battery.txt | grep "CAPACITY=" | tail -n1 | sed 's/=/     =      /g'

printf "\n"

#cat battery.txt | grep "STATUS=" | tail -n1 | head -n1 | sed 's/C/  C/g' | sed 's/I/   I/g' | sed 's/=/       =  /g'

cat battery.txt | grep "CHARGE_TYPE=" | tail -n1 | sed 's/=/  =     /g'

printf "\n"

cat battery.txt | grep "POWER_NOW=" | tail -n1 | sed 's/R_NOW=/R_NOW  =/g' | sed 's/=/  =   /g'

printf "\n"

cat battery.txt | grep "TEMP=" | tail -n1 | head -c9 | sed 's/=/         =      /g'

printf "\n"

cat battery.txt | grep "DIE_HEALTH=" | tail -n1 | sed 's/=/   =     /g'

cat battery.txt | grep " HEALTH=" | tail -n1 | sed 's/=/       =     /g'

printf "\n"

cat battery.txt | grep "CYCLE_COUNT=" | tail -n1 | sed 's/=/  =     /g'

#cat battery.txt | grep "CHARGE_FULL" | tail -n2 | sed 's/000$/ mAh/g' | sed 's/L=/L       =/g' | sed 's/CHARGE_//g' | sed 's/=/  =   /g'

printf "\n################################\n\n"

printf "\e[1;2mpress enter to quit\n\e[1;30m"

sudo rm -- battery.txt

read a
