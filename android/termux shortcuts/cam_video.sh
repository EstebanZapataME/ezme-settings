#!/bin/bash

OhMyHevc(){
for i in $(ls -1t | grep mp4)
do

    echo "Processing $i"
    nice ffmpeg -hwaccel "auto" -v "error" -stats -hide_banner \
    -i "$i" \
    -c:a libopus -ar 48000 -b:a 64K -sample_fmt s16 \
    -c:v libx265 -x265-params "log-level=error" -crf 25 -vf "hqdn3d,fps=fps=60" \
    $(basename "$i" p4)kv

    touch -c -t $(echo "$i" | head -c17 | tail -c13 | sed 's/_//g' ).$(echo "$i" | head -c19 | tail -c2) $(basename "$i" p4)kv

    beep
    echo ""
    sleep 0.5
done
}

echo "=================================================================================================="
echo "This represents how wide your screen should be"
echo "If it's not at least this wide, ffmpeg will wrap around"
echo "That will fill your screen with repeated progress reports"
echo "=================================================================================================="

echo ""
sleep 5

if [ -d /sdcard/DCIM/Camera ]
then
    cd /sdcard/DCIM/Camera
    echo "Optimizing Google Camera videos."
    echo "================================="
    OhMyHevc
    echo ""
    sleep 0.5
fi

if [ -d /sdcard/DCIM/OpenCamera ]
then
    cd /sdcard/DCIM/OpenCamera
    echo "Optimizing OpenCamera videos."
    echo "=============================="
    OhMyHevc
    echo ""
    sleep 0.5
fi

echo "Done"

beep
beep
beep

sleep 1
