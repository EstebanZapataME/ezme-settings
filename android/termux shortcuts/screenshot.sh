#!/bin/bash

printf "Optimizing the Screenshots\n"
printf "===========================\n\n"

cd /storage/emulated/0/Pictures/Screenshots

rename -a ' ' '_' *.png

for i in $(ls -1t | grep png | grep -v ",")
do

    nice oxipng -p -s -a -o3 "$i"

    touch -c -t $(echo "$i" | head -c24 | tail -c13 | sed 's/-//g').$(echo "$i" | head -c26 | tail -c2) "$i"

    mv "$i" $(basename "$i" .png),.png

    printf "\n." && sleep 0.33 && printf "." && sleep 0.33 && printf ".\n\n" && sleep 0.33

done

echo "Done"

beep && beep && beep
sleep 1
