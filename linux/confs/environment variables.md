# Environment Flags

These are flags which should be quite standard

----

## <code>PATH</code>

Where your executable files are located. This is a list of directories separated by a colon <code>:</code>. eg:

```bash
$HOME/.local/bin:/opt/program:/usr/local/bin:/usr/bin
```

----

## <code>LD_PRELOAD</code>

Lists libraries separated by a colon <code>:</code> that should override your regular libraries.

```bash
/opt/zlib-ng/libz.so:/opt/mozjpeg/libjpeg.so:/opt/mozjpeg/libturbojpeg.so
```

----

## <code>PREFIX</code>

Shows where to store and find files, commonly <code>/usr</code> but could be <code>/data/data/com.termux/files/usr</code>.

----

## <code>XDG_DATA_HOME</home>

This is an <i>actual</i> standard that is commonly just <code>$HOME/.local/share</home>.

----

## <code>XDG_CONFIG_HOME</home>

Another <i>actual</i> standard that is commonly just <code>$HOME/.config</home>.

----

## <code>XDG_CACHE_HOME</home>

Another <i>actual</i> standard that is commonly just <code>$HOME/.cache</home>.

----

## <code>HOSTAME</code>

The name of the current machine. Should be equal to the output of <code>hostname</code>.

----

## <code>PWD</code>

Your current directory. Should be equal to the output of <code>pwd</code>.

----

## <code>EDITOR</code>

Your prefered text editor. Could be <code>nano</code>, <code>vim</code>, <code>emacs</code>, etc.

----

## <code>TERM</code>

Which compatibility your shell provides. Some shells just lie and claim to be <code>xterm-256color</code> or similar.

----

## <code>SHELL</code>

Your command shell, like <code>bash</code>, <code>zsh</code>, <code>fish</code>, etc.

----

## <code>http_proxy</code>, <code>https_proxy</code> and <code>ftp_proxy</code>

They are the currently available proxies. They have a simple format, <code>https://USER:PASSWORD@PROXY_SERVER:PORT"</code> just like any other proxy.

