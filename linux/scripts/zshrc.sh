#!/bin/env zsh
# ~/.zshrc 
# Config mostly taken from https://github.com/eevee/rc/blob/master/.zshrc

#Make .zcompdump hidden???
export ZSH_COMPDUMP="$HOME/.zsh"

# Colorful
autoload colors; colors

# Tab Completion
_force_rehash() {
    (( CURRENT == 1 )) && rehash
    return 1
}
zstyle ':completion:*' menu select yes
zstyle ':completion:*:default' list-colors ''
zstyle ':completion:*' completer _force_rehash _complete _ignored _match _correct _approximate _prefix
zstyle ':completion:*' max-errors 2
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'r:|[._-]=** r:|=**'
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path ~/.zsh/cache
zstyle ':completion:*:descriptions' format "$fg_bold[black]» %d$reset_color"
zstyle ':completion:*' group-name ''
zstyle ':completion:*' ignored-patterns '*?.pyc' '__pycache__'
zstyle ':completion:*:*:rm:*:*' ignored-patterns
zstyle :compinstall filename "$HOME/.zshrc"
setopt complete_in_word
autoload -Uz compinit; compinit

# History
setopt extended_history hist_no_store hist_ignore_dups hist_expire_dups_first hist_find_no_dups inc_append_history share_history hist_reduce_blanks hist_ignore_space
export HISTFILE=~/.zsh_history
export HISTSIZE=1000000
export SAVEHIST=1000000

# Some.. options
setopt autocd beep extendedglob nomatch rc_quotes
unsetopt notify

# Don't count common path separators as word characters
WORDCHARS=${WORDCHARS//[&.;\/]}

# Tell how long a program has been running, without using time
REPORTTIME=5
TIMEFMT="Total time taken %*E"

# Don't glob with find or wget
for command in find wget; \
    alias $command="noglob $command"

# Keybindings
bindkey -e
bindkey "\e[1~" beginning-of-line
bindkey "\e[4~" end-of-line
bindkey "\e[5~" beginning-of-history
bindkey "\e[6~" end-of-history
bindkey "\e[3~" delete-char
bindkey "\e[2~" quoted-insert
bindkey "\e[1;5C" forward-word
bindkey "\e[1;5D" backward-word
bindkey "\e[5C" forward-word
bindkey "\eOc" emacs-forward-word
bindkey "\e[5D" backward-word
bindkey "\eOd" emacs-backward-word
bindkey "\e\e[C" forward-word
bindkey "\e\e[D" backward-word
bindkey "\e[8~" end-of-line
bindkey "\e[7~" beginning-of-line
bindkey "\eOH" beginning-of-line
bindkey "\eOF" end-of-line
bindkey "\e[H" beginning-of-line
bindkey "\e[F" end-of-line
bindkey '^i' complete-word
bindkey "\e[Z" reverse-menu-complete
down-line-or-local-history() {
    zle set-local-history 1
    zle down-line-or-history
    zle set-local-history 0
}
zle -N down-line-or-local-history
up-line-or-local-history() {
    zle set-local-history 1
    zle up-line-or-history
    zle set-local-history 0
}
zle -N up-line-or-local-history
bindkey "\e[A" up-line-or-local-history
bindkey "\eOA" up-line-or-local-history
bindkey "\e[B" down-line-or-local-history
bindkey "\eOB" down-line-or-local-history


# Color prompt
# name is cyan, @ is yellow, host is green, folder is blue and text is text
## Multiline?
## You need NerdFonts for this to work
precmd() {

  DirText=$(echo "$(pwd)" | \
    sed 's/^\/home\/uwu//' | \
    sed 's/^\/data\/data\/com.termux\/files\/home//' | \
    sed 's/^\/data\/data\/com.termux\/files\/usr//' | \
    sed 's/^\/sdcard//' | \
    sed 's/\/storage\/DEAD-BEEF//' | \
    sed 's/^\///' | \
    sed 's/\///g' )

if [ -d "/data/data/com.termux" ];
then
  echo "\e[1;2m\e[1;34m$DirText"
else
  echo "\e[1;36m$(whoami)\e[1;33m@\e[1;32m$HOST\e[1;2m\e[1;34m$DirText"
fi

}
PROMPT="%{$reset_color%}%(!.#.$) "

# Syntax Highlighting
if [ -d "$PATH/share/zsh/plugins/zsh-syntax-highlighting/" ] ; then
	source $PATH/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
fi

# Shell Common
if [[ -f $HOME/.local/bin/shell_common.sh ]]; then
    source $HOME/.local/bin/shell_common.sh
fi
