#!/bin/env bash
# ImageMagick may have low memory setup
# check /etc/ImageMagick-7/policy.xml and https://imagemagick.org/script/security-policy.php
# For Debian you can use https://www.deb-multimedia.org/ to have the latest version

noice=2
if [ "$1" != "" ]
then
    noice=$1
fi

echo ""
echo "Using noise $noice to enhance the images"
echo ""

SECONDS=0
mkdir -p $(pwd)/converted/
alias waifu2x=waifu2x-ncnn-vulkan

for i in $(ls -1Bvx | grep -v converted)
do
    magick convert -monitor $i -quality 100 -strip z.png
    oxipng -o1 z.png
    resol=$(magick identify -format "%h*%w" z.png)
    resal=$(($resol))
    echo "$(($resal / 1000000)) MP"
    echo ""
    waifu2x -n $noice -i z.png -o z.png
    resol=$(magick identify -format "%h*%w" z.png)
    resal=$(($resol))
    SECBAK=$SECONDS
    echo "  We are at $SECBAK seconds"
    echo "    $(($resal / 1000000)) MP"
    while [ $resal -lt 50000000 ]
    do
        echo ""
        waifu2x -i z.png -o z.png
        echo "  That was in $(($SECONDS - $SECBAK)) seconds"
        SECBAK=$SECONDS
        oxipng -o1 z.png
        resol=$(magick identify -format "%h*%w" z.png)
        resal=$(($resol))
        echo "    $(($resal / 1000000)) MP"
    done
    echo ""
    magick convert -monitor z.png -colorspace LAB -virtual-pixel dither -interpolate catrom -filter Mitchell -resize '32000000@' -clamp -strip -colorspace sRGB -quality 100 z.webp
    echo ""
    mv z.webp ./converted/$(crc32 z.webp).webp
    sleep 2
    gio trash -f $i
    gio trash -f z.png
    echo ""
    echo "You can CtrlC to close this now"
    beep
    sleep 1
    echo "."
    sleep 2
    echo ".."
    sleep 2
    echo "..."
    sleep 2
    echo "...."
    sleep 2
    echo "....."
    sleep 2
    echo "Starting next image"
    sleep 1
    echo ""
done

SECBAK=$SECONDS
echo "Finished in $SECBAK seconds"
SECH=$(( $SECBAK / 3600 ))
SECM=$(( $SECBAK / 60 - $SECH * 60 ))
SECS=$(( $SECBAK % 60 ))
echo "That's $SECH hours, $SECM minutes and $SECS seconds."
beep
beep
beep
echo ""
echo "Those were all of them"
echo ""
