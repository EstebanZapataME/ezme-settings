#!/bin/env bash

if [ $(command -v rhash) ]
then

   rhash -p '%{crc32c}\n' -- "$1"

fi
