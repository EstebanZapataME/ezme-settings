#!/bin/env bash
# Change the 4 and 3 to you number of threads

modprobe zram num_devices=4

for i in {0..3}
do
	echo zstd > /sys/block/zram"$i"/comp_algorithm
done

totalmem=`free | grep -e "^Mem:" | awk '{print $2}'`
mem=$(( ( $totalmem / 4 )* 1024))

for i in {0..3}
do
	echo $mem > /sys/block/zram"$i"/disksize
done

for i in {0..3}
do
	mkswap /dev/zram"$i"
	swapon -p 5 /dev/zram"$i"
done
