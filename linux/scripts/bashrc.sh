#!/bin/env bash
# ~/.bashrc

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# History
shopt -s histappend
shopt -s histverify
export HISTCONTROL=ignoreboth

# Limit how many directories are shown at once
PROMPT_DIRTRIM=2

# Color prompt
# name is cyan, @ is yellow, host is green, folder is blue and text is text
if [ -d "/data/data/com.termux" ];
then
    PS1="\[\e[1;34m\]\w\[\e[00m\]\$ "
else
    PS1="\[\e[1;36m\]\u\[\e[1;33m\]@\[\e[1;32m\]\h\[\e[00m\]:\[\e[1;34m\]\w\[\e[00m\]\$ "
fi

# colored GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'


# Shell common
if [ -f "$HOME/.local/bin/shell_common.sh" ]; then
    source $HOME/.local/bin/shell_common.sh
fi

