#!/bin/env bash
# ~/.local/bin/shell_common.sh
# I didn't want to duplicate bash and zsh code
# This is mostly ENVironment variables and PATHs

if [ -z "$PREFIX" ]
then
   export PREFIX="/usr"
fi

# XDG Base Directory Specification
if [ -z "$XDG_DATA_HOME" ]
then
   export XDG_DATA_HOME="$HOME/.local/share"
fi

if [ -z "$XDG_CONFIG_HOME" ]
then
   export XDG_CONFIG_HOME="$HOME/.config"
fi

if [ -z "$XDG_STATE_HOME" ]
then
   export XDG_STATE_HOME="$HOME/.local/state"
fi

if [ -z "$XDG_DATA_DIRS" ]
then
   export XDG_DATA_DIRS="$PREFIX/local/share:$PREFIX/share"
fi

if [ -z "$XDG_CACHE_HOME" ]
then
   export XDG_CACHE_HOME="$HOME/.cache"
fi

# PyPy path
if [ -d "$PREFIX/opt/pypy3/bin" ]
then
    PATH="$PREFIX/opt/pypy3/bin:$PATH"
fi

# user's private bin
if [ -d "$HOME/.local/bin" ]
then
    PATH="$HOME/.local/bin:$PATH"
fi

# Alias definitions
if [ -f "$HOME/.local/bin/shell_aliases.sh" ]
then
    source $HOME/.local/bin/shell_aliases.sh
fi

# Some defaults that aren't always there
export HOSTNAME=$(hostname)
export EDITOR=nano
export SUDO_EDITOR=rnano

# theFuck helper
if  ( command -v fuck &> /dev/null )
then
	eval "$(thefuck --alias)"
fi

# Fix a pkg-config "feature"
if  ( command -v pkgconfig &> /dev/null )
then
	export PKG_CONFIG_PATH="$(pkg-config --variable pc_path pkg-config)"
fi

# Make MkvToolNix engage in experimental stuff
export MTX_ENGAGE="write_headers_twice,space_after_chapters"

# DNS
export dns_tls="cloudflare-dns.com"
export dns_https="https://cloudflare-dns.com/dns-query"

# User Agent
## Chrome
export UA_Android_Chrome="Mozilla/5.0 (Linux; Android 10; K) (KHTML, like Gecko) Chrome/114.0.0.0 Mobile"
export UA_Linux_Chrome="Mozilla/5.0 (Wayland; Linux x86_64) (KHTML, like Gecko) Chrome/114.0.0.0"
export UA_Windows_Chrome="Mozilla/5.0 (Windows NT 10.0; Win64; x64) (KHTML, like Gecko) Chrome/114.0.0.0"
## Firefox
export UA_Android_Firefox="Mozilla/5.0 (Android 10; Mobile; rv:114.0) Gecko/114.0 Firefox/114.0"
export UA_Linux_Firefox="Mozilla/5.0 (Wayland; Linux x86_64; rv:114.0) Gecko/20100101 Firefox/114.0"
export UA_Windows_Firefox="Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:114.0) Gecko/20100101 Firefox/114.0"
## Other
export UA_Tor="Mozilla/5.0 (Windows NT 10.0; rv:91.0) Gecko/20100101 Firefox/91.0"
export UA_cURL="curl/8.1.2"
export UA_GoogleBot="Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)"


# Other libraries
## zlib-ng
#export LD_PRELOAD="$PREFIX/opt/zlib-ng/libz.so:$LD_PRELOAD" #Breaks many packages :(
#export LD_PRELOAD="$PREFIX/opt/zlib-ng/libz-ng.so:$LD_PRELOAD"
## mozjpeg
#export LD_PRELOAD="$PREFIX/opt/mozjpeg/libjpeg.so:$LD_PRELOAD"
#export LD_PRELOAD="$PREFIX/opt/mozjpeg/libturbojpeg.so:$LD_PRELOAD"

# Lynx
export LYNX_CFG="$XDG_CONFIG_HOME/lynx.cfg"

