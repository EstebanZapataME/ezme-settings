#!/bin/env bash

if [ -x "$PREFIX/bin/rpm2cpio" ]; then
	rpm2cpio "$1" | cpio -imdv
else
    rpm2cpio.sh "$1" | cpio -imdv
fi
