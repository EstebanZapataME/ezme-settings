#!/bin/env bash

usage() {
echo "ez-thumb.sh [image.jpg]"
echo ""
echo "automatically adds a thumbnail to the selected jpg"
exit
}

main() {
Imagen=$(basename "$1" .jpg)

magick convert "$1" -filter Box -resize '5%' -clamp -strip "$Imagen"-thumb.jpg

jpegoptim -q -s --all-normal -m50 "$Imagen"-thumb.jpg

exiv2 -k -dt rm "$1"
exiv2 -k -it insert "$1"

rm "$Imagen"-thumb.jpg

exit
}

while [ "$1" != "" ];
do
case $1 in
h | help ) usage
           ;;
* )        main
esac
done

if [ "$1" == "" ]
then
    usage
fi
