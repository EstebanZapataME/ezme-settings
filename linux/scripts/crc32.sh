#!/bin/env bash

if [ $(command -v rhash) ]
then

   rhash -p '%{crc32}\n' -- "$1"

else

   if [ $(command -v pigz) ]
   then
      Pig="pigz -0"
   else
      Pig="gzip -1"
   fi

   cat $1 | $Pig -c | tail -c8 | od -t x4 -N 4 -A n | tail -c9

fi
