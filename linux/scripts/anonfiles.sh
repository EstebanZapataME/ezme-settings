#!/bin/env bash

export files=$2

usage(){
  printf "AnonFiles File Uploader\n"
  printf "anonfiles.sh [site] [file]\n\n"
  printf "Choose from these sites:\n"
  printf "\e[1;1manon\e[1;0mfiles, \e[1;1mbay\e[1;0mfiles, file\e[1;1mchan\e[1;0m, \e[1;1mhot\e[1;0mfile, \e[1;1mlets\e[1;0mupload, \e[1;1mlola\e[1;0mbits, \n"
  printf "\e[1;1mmega\e[1;0mupload, \e[1;1mmy\e[1;0mfile, \e[1;1mopen\e[1;0mload, \e[1;1mrapid\e[1;0mshare, \e[1;1mup\e[1;0mvid, \e[1;1mv\e[1;0mshare\n"
  exit 1
}

upload(){
  curl --progress-bar -F file=@"$files" https://api."$url"/upload | tee /dev/null
  exit 0
}

while [ "$1" != "" ];
do
case $1 in
h | help )            usage
                      ;;
anon | anonfiles )    url=anonfiles.com
                      upload
                      ;;
bay | bayfiles )      url=bayfiles.com
                      upload
                      ;;
chan | filechan )     url=filechan.org
                      upload
                      ;;
hot | hotfile )       url=hotfile.io
                      upload
                      ;;
lets | letsupload )   url=letsupload.cc
                      upload
                      ;;
lola | lolabits )     url=lolabits.se
                      upload
                      ;;
mega | megaupload )   url=megaupload.nz
                      upload
                      ;;
my | myfile )         url=myfile.is
                      upload
                      ;;
open | openload )     url=openload.cc
                      upload
                      ;;
rapid | rapidshare )  url=rapidshare.nu
                      upload
                      ;;
share | shareonline ) url=share-online.is
                      upload
                      ;;
up | upvid )          url=upvid.cc
                      upload
                      ;;
v | vshare )          url=vshare.is
                      upload
                      ;;
* )                   usage
esac
done

if [ "$1" == "" ]
then
usage
fi

