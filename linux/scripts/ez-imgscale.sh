#!/bin/env bash

pikture="$1"

resolucion='100%'
if [ "$2" != "" ]
then
    resolucion=$2
fi

usage() {
echo "usage:"
echo "ez-imgscale [image] [resolution] [filter]"
echo ""
echo "Resolution can be WxH, area@, scale%, aspect:ratio, scaleX/scaleY%"
echo "If let empty, it assumes original size (100%)"
echo ""
printf "Filter can be \e[1;1mb\e[1;0mox, \e[1;1mg\e[1;0maussian, \e[1;1ml\e[1;0manczos, \e[1;1mm\e[1;0mitchell\n, or \e[1;1mr\e[1;0mobidoux."
echo "If let empty, it uses Mitchell."
exit 1
}


use-box() {
magick convert -monitor $pikture -colorspace LAB -virtual-pixel dither -interpolate catrom -filter Box -resize "$resolucion" -clamp -strip -colorspace sRGB -quality 100 $pikture
exit
}

use-gaussian() {
magick convert -monitor $pikture -colorspace LAB -virtual-pixel dither -interpolate catrom -filter Gaussian -define filter:blur=0.75 -resize "$resolucion" -clamp -strip -colorspace sRGB -quality 100 $pikture
exit
}

use-lanczos() {
magick convert -monitor $pikture -colorspace LAB -virtual-pixel dither -interpolate catrom -filter Lanczos -define filter:window=catrom -resize "$resolucion" -clamp -strip -colorspace sRGB -quality 100 $pikture
exit
}

use-mitchell() {
magick convert -monitor $pikture -colorspace LAB -virtual-pixel dither -interpolate catrom -filter Mitchell -resize "$resolucion" -clamp -strip -colorspace sRGB -quality 100 $pikture
exit
}

use-robidoux() {
magick convert -monitor $pikture -colorspace LAB -virtual-pixel dither -interpolate catrom -filter Robidoux -distort Resize "$resolucion" -clamp -strip -colorspace sRGB -quality 100 $pikture
exit
}


while [ "$3" != "" ];
do
case $3 in
h | help )      usage
                ;;
b | box )       use-box
                ;;
g | gaussian )  use-gaussian
                ;;
l | lanczos )   use-lanczos
                ;;
m | mitchell )  use-mitchell
                ;;
r | robidoux )  use-robidoux
                ;;
* )             usage
esac
done

if [ "$1" == "" ]
then
    usage
fi

if [ "$3" == "" ]
then
    use-mitchell
fi

