#!/bin/env bash

usage() {
echo "usage:"
echo "ez-vidcomp [input] [type] [rate]"
echo ""
echo "type can be h265 or vp9"
echo "rate can be a number from 50 (worst) to 0 (best)"
exit 1
}

hevc() {
SECONDS=0
ffmpeg -hide_banner -hwaccel auto -stats -v error -i "$Input" -c:v libx265 -x265-params "log-level=error" -b:v 0 -crf "$Rate" -pass 1 -an -f null /dev/null && \
ffmpeg -hide_banner -hwaccel auto -stats -v error -i "$Input" -c:v libx265 -x265-params "log-level=error" -b:v 0 -crf "$Rate" -pass 2 -an $(echo "$Input").mkv
SECBAK=$SECONDS
echo "Finished in $SECBAK seconds"
SECH=$(( $SECBAK / 3600 ))
SECM=$(( $SECBAK / 60 - $SECH * 60 ))
SECS=$(( $SECBAK % 60 ))
echo "That's $SECH hours, $SECM minutes and $SECS seconds."
beep
exit
}

vp9() {
SECONDS=0
ffmpeg -hide_banner -hwaccel auto -stats -v error -i "$Input" -c:v libvpx-vp9 -b:v 0 -crf "$Rate" -row-mt 1 -pass 1 -an -f null /dev/null && \
ffmpeg -hide_banner -hwaccel auto -stats -v error -i "$Input" -c:v libvpx-vp9 -b:v 0 -crf "$Rate" -row-mt 1 -pass 2 -an $(echo "$Input").webm
SECBAK=$SECONDS
echo "Finished in $SECBAK seconds"
SECH=$(( $SECBAK / 3600 ))
SECM=$(( $SECBAK / 60 - $SECH * 60 ))
SECS=$(( $SECBAK % 60 ))
echo "That's $SECH hours, $SECM minutes and $SECS seconds."
beep
exit
}

export Input=$2
export Rate=$4

while [ "$2" != "" ];
do
case $2 in
h | h265 )   hevc
             ;;
hevc )       hevc
             ;;
v | vp9 )    vp9
             ;;
* )          usage
esac
done

if [ "$1" == "" ]
then
usage
fi
