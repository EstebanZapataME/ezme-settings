#!/bin/env bash
# ~/.local/bin/shell_aliases.sh
# Compatible with Bash, and Zsh

# Privilege Escalation
if  ( command -v doas &> /dev/null )
then
   alias doas="doas "
   export PrivTool="doas"
   alias sudo="$PrivTool "
   alias sudoedit="$PrivTool rnano"
fi

# Android (termux)
if [ -d /data/data/com.termux ]
then
   alias cdsd="cd /sdcard/"
   alias cd0="cd /storage/emulated/0/"
   alias cdext="cd /storage/DEAD-BEEF"
   alias cdusr="cd $PREFIX"
   alias termux-elf-cleaner="termux-elf-cleaner --api-level 31" #12L
   alias build-date="date -Is -d @$(getprop | grep ro.system.build.date.utc | tail -c12 | sed -u 's/\]//')"
   export PrivTool=""
fi

# Internals
alias ls="ls -1 -h --color=auto --group-directories-first"
alias ls-s="ls -1 -s -h --color=auto --group-directories-first"
alias ls-gg="ls -Gg -h --color=auto --group-directories-first"
alias cls="clear"
alias rm="rm -r --preserve-root"
alias cp="cp -r"
alias mv="mv -n"
alias lsblk="lsblk -o NAME,FSTYPE,LABEL,FSUSE%,FSSIZE,MOUNTPOINT"
alias t1901="touch -t 190112132045.52 -c"
alias t1970="touch -t 197001010000.00 -c"
alias t2038="touch -t 203801190314.07 -c"
alias t2106="touch -t 210614070628.15 -c"
alias grep="grep --color=auto"
alias greper="grep --color=always -s -n -H -r"
alias dd="dd status=progress"

if [ ! -n "$MSYSTEM" ]
then
   alias clear="printf '\e[H\e[3J\e[2J'" 
fi

if  ( ! command -v hostname &> /dev/null )
then
   alias hostname="cat /etc/hostname"
fi

# Downloaders
alias wget="wget --progress=bar:noscroll --show-progress -w 5 --random-wait --no-dns-cache"
alias axel="axel -a"
alias curl="curl --progress-bar"
alias rsget="rsget -u"

# Package Manager Helpers
## debian
alias pat-install="$PrivTool apt install"
alias pat-remove="$PrivTool apt remove"
alias pat-purge="$PrivTool apt purge"
alias pat-update="$PrivTool apt update"
alias pat-upgrade="$PrivTool apt update && $PrivTool apt upgrade"
alias pat-check="$PrivTool apt-get check"
alias pat-autoremove="$PrivTool apt autoremove"
alias pat-autopurge="$PrivTool apt-get autopurge"
alias pat-show="apt show"
alias pat-search="apt search"
alias pat-clean="$PrivTool apt-get clean"
alias pat-check="$PrivTool apt-get check"
alias pat-fix="$PrivTool apt-get install -fix"
alias pat-cow="apt moo"
## arch
alias cap-install="$PrivTool pacman -S"
alias cap-fileinstall="$PrivTool pacman -U"
alias cap-remove="$PrivTool pacman -Rs"
alias cap-check="pacman -Dk"
alias cap-search="pacman -Ss"
alias cap-show="pacman -Qi"
alias cap-list="pacman -Qet"
alias cap-upgrade="$PrivTool pacman -Syu"
alias cap-clean="$PrivTool pacman -Sc"
alias cap-owner="pacman -Qo"
alias cap-autoremove="pacman -Qtdq | $PrivTool pacman -Rns - "
alias cap-repair="$PrivTool pacman -Fy && pacman -Qnq | $PrivTool pacman -S - "
alias cap-aurepair="pikaur -Qmq | pikaur -S - "

# EZ Tools
alias ez-ports="$PrivTool ss -tulpn"
alias ez-defrag="$PrivTool btrfs filesystem defragment -v -r -f"

# EZFS MKFS
alias ezfs-btrfs="$PrivTool mkfs.btrfs -R free-space-tree"
alias ezfs-btrfs-64k="$PrivTool mkfs.btrfs -R free-space-tree -n 65536"
## fat32 (use -4k for larger drives)
alias ezfs-fat32="$PrivTool mkfs.vfat -F 32 -S 512 -b 0"
alias ezfs-fat32-4k="$PrivTool mkfs.vfat -F 32 -S 4096 -b 0"
## ntfs (use -4k for larger drives) (Cluster, Sector)
alias ezfs-ntfs="$PrivTool mkfs.ntfs -I -c 512 -s 512 -z 4 -f"
alias ezfs-ntfs-4k="$PrivTool mkfs.ntfs -I -c 4096 -s 4096 -z 1 -f"
## exfat (drive size =< value) (Boundary, Cluster)
alias ezfs-exfat-1g="$PrivTool mkfs.exfat --pack-bitmap -b 64K -c 16K"
alias ezfs-exfat-2g="$PrivTool mkfs.exfat --pack-bitmap -b 64K -c 32K"
alias ezfs-exfat-32g="$PrivTool mkfs.exfat --pack-bitmap -b 4M -c 32K"
alias ezfs-exfat-128g="$PrivTool mkfs.exfat --pack-bitmap -b 16M -c 128K"
alias ezfs-exfat-512g="$PrivTool mkfs.exfat --pack-bitmap -b 32M -c 256K"
alias ezfs-exfat-2t="$PrivTool mkfs.exfat --pack-bitmap -b 64M -c 512K"

# ncnn-vulkan
alias realcugan="nice realcugan-ncnn-vulkan"
alias realsr="nice realsr-ncnn-vulkan"
alias irfnet="nice irfnet-ncnn-vulkan"

# mkvtoolnix
alias mkvpropedit="mkvpropedit --flush-on-close "
alias mkvmerge="mkvmerge --flush-on-close --clusters-in-meta-seek "

# lsd
alias lsd-s="lsd -l --blocks size,name"
alias lsd-gg="lsd -l --blocks permission,size,date,name"

# Images
alias pngout="nice pngout -n1 -ks -r"
alias pngquant="nice pngquant -s --speed=1"
alias optipng="nice optipng -clobber -preserve -fix -zw 32k -strip all"
alias oxipng="nice oxipng -p -s -a --fix"
alias jpegoptim="nice jpegoptim -p -P -t --all-normal"
alias cwebp="nice cwebp -z 9 -m 6 -sharp_yuv -mt -short -progress"
alias gif2webp="nice gif2webp -m 6 -mt -mixed -mix_size"
alias gifski="nice gifski --extra"

# Other
alias ffmpeg="nice ffmpeg -hide_banner -hwaccel auto -v error -stats"
alias kzip="nice kzip -rn -n1 -r"
alias wwt="$PrivTool nice wwt --256-colors -v -P"
alias dog="dog --time --color=auto"
alias update-grub="$PrivTool grub-mkconfig -o /boot/grub/grub.cfg"
alias tldr="tldr -p linux"

# Bat
batdiff() {
    git diff --name-only --relative --diff-filter=d | xargs bat --diff
}

batman() {
    man "$@" | col -bx | bat -l man -p
}
