#!/bin/env bash

usage() {
echo "usage:"
echo "ez-archive [command] [folder to archive]"
echo ""
echo "commands:"
echo "h: display this help"
echo "==for ZIP=="
echo "s | storezip: only store with zip"
echo "d | zip: compress with Deflate"
echo "b | bzip2: compress with bzip2"
echo "k | kzip: compress with kzip (KSflate)"
echo "z | zopfli: compress with Zopfli"
echo "==for 7z=="
echo "7z: compress with 7z (LZMA2)"
echo "==for TAR=="
echo "tar: only store with tar"
echo "tarxz: compress with LZMA2"
echo "tarzst: compress with ZStd"
exit 1
}

storemyzip() {
zip -r -0 "$files".zip -- "$files"
exit
}

bee2myzip() {
zip -r -Z bzip2 -9 "$files".b.zip -- "$files"
exit
}

deflatemyzip() {
zip -r -9 "$files".d.zip -- "$files"
exit
}

kenzip() {
kzip -r -s0 -b0 -rn "$files".k.zip -- "$files"
exit
}

pozilf() {
advzip -a -p -4 -i=1 "$files".z.zip -- "$files"
exit
}

makemea7z() {
7zr a -t7z -m0=lzma2 -md=64m -ms=on -mfb=273 -mx=9 -myx=9  "$files".7z -- "$files"
exit
}

takeatar() {
tar cf "$files".tar -- "$files"
exit
}

lzmamytar() {
tar cfS "$files".txz --use-compress-program="xz -v -T 0 --lzma2=preset=9e,dict=64MiB,nice=273,lc=4,lp=0,pb=0,depth=0 " -- "$files"
exit
}

zstdmytar() {
tar cfS "$files".tar.zst --use-compress-program="pzstd -v " -- "$files"
exit
}

while [ "$1" != "" ];
do
case $1 in
h | help )          usage
                    ;;
s | storezip )      files=$2
                    storemyzip
                    ;;
d | zip )           files=$2
                    deflatemyzip
                    ;;
b | bzip2 )         files=$2
                    bee2myzip
                    ;;
k | kzip )          files=$2
                    kenzip
                    ;;
z | zopfli )        files=$2
                    pozilf
                    ;;
lzma2 | 7z )        files=$2
                    makemea7z
                    ;;
t | tar )           files=$2
                    takeatar
                    ;;
xz | tarxz )        files=$2
                    lzmamytar
                    ;;
zstd | tarzst )     files=$2
                    zstdmytar
                    ;;
* )                 usage
esac
done

if [ "$1" == "" ]
then
usage
fi
