@echo off

PowerShell -ExecutionPolicy Unrestricted -Command " $installer = (Get-ChildItem \"$env:ProgramFiles*\Microsoft\Edge\Application\*\Installer\setup.exe\"); if (!$installer) { Write-Host Could not find the installer; } else { & $installer.FullName -uninstall -system-level -verbose-logging -force-uninstall }; "
