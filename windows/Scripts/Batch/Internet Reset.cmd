@echo off

netsh winsock reset
netsh int ip reset
netcfg -d

ipconfig /flushdns
ipconfig /release
ipconfig /release6
ipconfig /renew
ipconfig /renew6
ipconfig /registerdns
