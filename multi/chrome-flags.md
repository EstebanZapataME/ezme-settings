## GPU

`enable-accelerated-video-decode` ✅Enable

`enable-gpu-rasterization` ✅Enable

`enable-vulkan` ✅Enable

`enable-zero-copy` ✅Enable

`ignore-gpu-blocklist` ✅Enable

----

## Privacy and Security

`abusive-notification-permission-revocation` ✅Enabled

`dns-httpssvc` ✅Enabled

`enable-navigation-predictor` ❌Disable

`enable-preconnect-to-search` ❌Disable

`enable-save-data` ❌Disable

`enable-subresource-redirect` ❌Disable

`enable-quic` ✅Enable

`enable-webrtc-hide-local-ips-with-mdns` ✅Enable

`fill-on-account-select` ✅Enable

`http-cache-partitioning` ✅Enabled

`legacy-tls-enforced` ✅Enabled

`omnibox-default-typed-navigations-to-https` ✅Enable

`post-quantum-cecpq2` ✅Enable

`privacy-advisor` ✅Enable

`quiet-notification-prompts` ✅Enable

`restrict-gamepad-access` ✅Enable

----

## Performance

`animated-image-resume` ✅Enable

`audio-worklet-realtime-thread` ✅Enable

`compositor-threaded-scrollbar-scrolling` ✅Enable

`delay-competing-low-priority-requests` ✅Enable

`ntp-cache-one-google-bar` ❌Disable

`enable-defer-all-script` ✅Enable

`enable-heavy-ad-intervention` ✅Enable

`enable-lazy-frame-loading` ✅Enable (Automatically)

`enable-lazy-image-loading` ✅Enable (Automatically)

`enable-lite-video` ✅Enable

`enable-new-download-backend` ✅Enable

`enable-oop-print-drivers` ✅Enable

`enable-parallel-downloading` ✅Enable

`enable-policy-blocklist-throttle-requires-policies-loaded` ✅Enable

`enable-sync-requires-policies-loaded` ✅Enable

`enable-throttle-display-none-and-visibility-hidden-cross-origin-iframes` ✅Enable

`enable-webassembly-threads` ✅Enable

`intensive-wake-up-throttling` ✅Enable (10 secs)

`turn-off-streaming-media-caching-always` ✅Enable

`webxr-incubations` ❌Disable

`webxr-runtime` ❌No Runtime

----

## PDF viewer

`pdf-viewer-document-properties` ✅Enable

`pdf-viewer-presentation-mode` ✅Enable

----

## Personal taste

`content-settings-redesign` ✅Enable

`edit-passwords-in-settings` ✅Enable

`enable-login-detection` ✅Enable

`enable-new-profile-picker` ✅Enable

`enable-pixel-canvas-recording` ✅Enable

`force-color-profile` sRGB

`PasswordImport` ✅Enable

`pull-to-refresh` ❌Disable

`tab-groups-auto-create` ❌Disable

`username-first-flow` ✅Enable
