/* These are the only options that you could consider changing */

user_pref("browser.search.region", "US");
user_pref("intl.accept_languages", "en-US, en");
user_pref("javascript.use_us_english_locale", true);


/* The following options shouldn't really be changed */

/* disable WebAssembly */
/* If you do use it, then you can enable it, but it leaves you open to cryptominers */
user_pref("javascript.options.wasm", false);

/* Enable userContent.css and userChrome.css */
user_pref("toolkit.legacyUserProfileCustomizations.stylesheets", true);

/* Enable "View Image Info" */
user_pref("browser.menu.showViewImageInfo", true);

/* disable about:config warning */
user_pref("browser.aboutConfig.showWarning", false);

/*  disable default browser check */
user_pref("browser.shell.checkDefaultBrowser", false);

/* all about:blank */
user_pref("browser.startup.page", 0);
user_pref("browser.startup.homepage", "about:blank");
user_pref("browser.newtabpage.enabled", false);
user_pref("browser.newtab.preload", false);

/* no newtab stuff  */
user_pref("browser.newtabpage.activity-stream.feeds.telemetry", false);
user_pref("browser.newtabpage.activity-stream.telemetry", false);
user_pref("browser.newtabpage.activity-stream.feeds.snippets", false);
user_pref("browser.newtabpage.activity-stream.feeds.section.topstories", false);
user_pref("browser.newtabpage.activity-stream.section.highlights.includePocket", false);
user_pref("browser.newtabpage.activity-stream.showSponsored", false);
user_pref("browser.newtabpage.activity-stream.feeds.discoverystreamfeed", false);
user_pref("browser.newtabpage.activity-stream.showSponsoredTopSites", false);

/* clear default topsites */
user_pref("browser.newtabpage.activity-stream.default.sites", "");

/* use Mozilla geolocation service instead of Google */
user_pref("geo.provider.network.url", "https://location.services.mozilla.com/v1/geolocate?key=%MOZILLA_API_KEY%");

/* disable using the OS's geolocation service */
user_pref("geo.provider.ms-windows-location", false);
user_pref("geo.provider.use_corelocation", false);
user_pref("geo.provider.use_gpsd", false);

/* disable region updates */
user_pref("browser.region.network.url", "");
user_pref("browser.region.update.enabled", false);

/* disable addon recommendations */
user_pref("extensions.getAddons.showPane", false);
user_pref("extensions.htmlaboutaddons.recommendations.enabled", false);
user_pref("browser.discovery.enabled", false);

/* disable Studies */
user_pref("app.shield.optoutstudies.enabled", false);
user_pref("app.normandy.enabled", false);
user_pref("app.normandy.api_url", "");

/* disable Captive Portal detection */
user_pref("captivedetect.canonicalURL", "");
user_pref("network.captive-portal-service.enabled", false);

/* disable Network Connectivity checks */
user_pref("network.connectivity-service.enabled", false);

/* disable Safe Browsing */
user_pref("browser.safebrowsing.malware.enabled", false);
user_pref("browser.safebrowsing.phishing.enabled", false);
user_pref("browser.safebrowsing.downloads.enabled", false);
user_pref("browser.safebrowsing.downloads.remote.enabled", false);
user_pref("browser.safebrowsing.downloads.remote.url", "");
user_pref("browser.safebrowsing.downloads.remote.block_potentially_unwanted", false);
user_pref("browser.safebrowsing.downloads.remote.block_uncommon", false);
user_pref("browser.safebrowsing.allowOverride", false);

/* disable prefetching */
user_pref("network.prefetch-next", false);
user_pref("network.dns.disablePrefetch", true);
user_pref("network.dns.disablePrefetchFromHTTPS", true);
user_pref("network.predictor.enabled", false);
user_pref("network.predictor.enable-prefetch", false);
user_pref("network.http.speculative-parallel-limit", 0);
user_pref("browser.places.speculativeConnect.enabled", false);

/* disable click jacking */
user_pref("browser.send_pings", false);

/* disable IPv6 */
user_pref("network.dns.disableIPv6", true);

/* socks proxy should do dns */
user_pref("network.proxy.socks_remote_dns", true);

/* disable using UNC */
user_pref("network.file.disable_unc_paths", true);

/* disable GIO as a potential proxy bypass vector */
user_pref("network.gio.supported-protocols", "");

/* disable location bar domain guessing */
user_pref("browser.fixup.alternate.enabled", false);

/* disable location bar making speculative connections */
user_pref("browser.urlbar.speculativeConnect.enabled", false);

/* disable leaking single words to a DNS provider */
user_pref("browser.urlbar.dnsResolveSingleWordsAfterSearch", 0);

/* disable location bar contextual suggestions */
user_pref("browser.urlbar.suggest.quicksuggest.nonsponsored", false);
user_pref("browser.urlbar.suggest.quicksuggest.sponsored", false);

/* always prompt for the primary password */
user_pref("security.ask_for_password", 2);
user_pref("security.password_lifetime", 5);

/* Disable auto fill */
user_pref("signon.autofillForms", false);

/* disable formless login capture for Manager */
user_pref("signon.formlessCapture.enabled", false);

/* disable HTTP authentication credentials dialogs triggered by sub-resources */
user_pref("network.auth.subresource-http-auth-allow", 0);

/* enforce no automatic auth on Microsoft sites */
user_pref("network.http.windows-sso.enabled", false);

/* disable media cache from writing to disk in Private Browsing */
user_pref("browser.privatebrowsing.forceMediaMemoryCache", true);

/* disable storing extra session data */
user_pref("browser.sessionstore.privacy_level", 2);

/* less session restore */
user_pref("browser.sessionstore.interval", 30000);

/* disable automatic restart after reboot */
user_pref("toolkit.winRegisterApplicationRestart", false);

/* require safe negotiation */
user_pref("security.ssl.require_safe_negotiation", true);

/* disable TLS1.3 0-RTT */
user_pref("security.tls.enable_0rtt_data", false);

/* enforce OCSP fetching */
user_pref("security.OCSP.enabled", 1);

/* set OCSP fetch failures to hard-fail */
user_pref("security.OCSP.require", true);

/* disable Microsoft Family Safety cert */
user_pref("security.family_safety.mode", 0);

/* enable strict pinning */
user_pref("security.cert_pinning.enforcement_level", 2);

/* enable CRLite */
user_pref("security.remote_settings.crlite_filters.enabled", true);
user_pref("security.pki.crlite_mode", 2);

/* disable insecure passive content */
user_pref("security.mixed_content.block_display_content", true);

/* enable HTTPS-Only mode in all windows */
user_pref("dom.security.https_only_mode", true);

/* display warning on the padlock for "broken security" */
user_pref("security.ssl.treat_unsafe_negotiation_as_broken", true);

/* Add Security Exception dialog on SSL warnings */
user_pref("browser.ssl_override_behavior", 1);

/* display info about Insecure Connection warning */
user_pref("browser.xul.error_pages.expert_bad_cert", true);

/* disable rendering of SVG OpenType fonts */
user_pref("gfx.font_rendering.opentype_svg.enabled", false);

/* limit font visibility */
user_pref("layout.css.font-visibility.private", 1);
user_pref("layout.css.font-visibility.standard", 1);
user_pref("layout.css.font-visibility.trackingprotection", 1);

/* control when to send a cross-origin referer */
user_pref("network.http.referer.XOriginPolicy", 2);

/* control the amount of cross-origin info to send */
user_pref("network.http.referer.XOriginTrimmingPolicy", 2)

/* enable Container Tabs and its UI setting */
user_pref("privacy.userContext.enabled", true);
user_pref("privacy.userContext.ui.enabled", true);

/* force WebRTC inside the proxy */
user_pref("media.peerconnection.ice.proxy_only_if_behind_proxy", true);

/* force a single network interface for ICE candidates generation */
user_pref("media.peerconnection.ice.default_address_only", true);

/* disable autoplay of HTML5 media */
user_pref("media.autoplay.default", 5);

/* disable autoplay of HTML5 media if you interacted with the site */
user_pref("media.autoplay.blocking_policy", 2);

/* disable "Confirm you want to leave" dialog on page close */
user_pref("dom.disable_beforeunload", true);

/* prevent scripts from moving/resizing open windows */
user_pref("dom.disable_window_move_resize", true);

/* block popup windows */
user_pref("dom.disable_open_during_load", true);

/* limit events that can cause a popup */
user_pref("dom.popup_allowed_events", "click dblclick mousedown pointerdown");

/* prevent accessibility services from accessing your browser */
user_pref("accessibility.force_disabled", 1);

/* disable sending additional analytics to web servers */
user_pref("beacon.enabled", false);

/* remove temp files opened with an external app */
user_pref("browser.helperApps.deleteTempFileOnExit", true);

/* disable page thumbnail collection */
user_pref("browser.pagethumbnails.capturing_disabled", true);

/* disable UITour backend */
user_pref("browser.uitour.enabled", false);
user_pref("browser.uitour.url", "");

/* disable various developer tools in browser context */
user_pref("devtools.chrome.enabled", false);
user_pref("devtools.debugger.remote-enabled", false);

/* disable middle mouse opening links from clipboard */
user_pref("middlemouse.contentLoadURL", false);

/* use Punycode */
user_pref("network.IDN_show_punycode", true);

/* enforce PDFJS, disable PDFJS scripting */
user_pref("pdfjs.disabled", false);
user_pref("pdfjs.enableScripting", false);

/* disable links launching Windows Store */
user_pref("network.protocol-handler.external.ms-windows-store", false);

/* disable permissions delegation */
user_pref("permissions.delegation.enabled", false);

/* lock down allowed extension directories */
user_pref("extensions.enabledScopes", 5);
user_pref("extensions.autoDisableScopes", 15);

/* disable bypassing extension install prompts */
user_pref("extensions.postDownloadThirdPartyPrompt", false);

/* enable state partitioning of service workers */
user_pref("privacy.partition.serviceWorkers", true);

/* disable asm.js */
user_pref("javascript.options.asmjs", false);

/* no JIT */
user_pref("javascript.options.ion", false);
user_pref("javascript.options.baselinejit", false);
user_pref("javascript.options.jit_trustedprincipals", true);

/* Disabling of Web Compatibility Reporter */
user_pref("extensions.webcompat-reporter.enabled", false);

/* disable SHA-1 certificates */
user_pref("security.pki.sha1_enforcement_level", 1);

/* disable non-modern cipher suites */
user_pref("security.ssl3.ecdhe_ecdsa_aes_256_sha", false);
user_pref("security.ssl3.ecdhe_ecdsa_aes_128_sha", false);
user_pref("security.ssl3.ecdhe_rsa_aes_128_sha", false);
user_pref("security.ssl3.ecdhe_rsa_aes_256_sha", false);
user_pref("security.ssl3.rsa_aes_128_gcm_sha256", false);
user_pref("security.ssl3.rsa_aes_256_gcm_sha384", false);
user_pref("security.ssl3.rsa_aes_128_sha", false);
user_pref("security.ssl3.rsa_aes_256_sha", false);
user_pref("security.ssl3.rsa_des_ede3_sha", false); 

/* enable the DNT (Do Not Track) HTTP header */
user_pref("privacy.donottrackheader.enabled", true);

/* Make Cursive fonts work on fontconfig */
user_pref("font.name.cursive.x-math", "cursive");
user_pref("font.name.cursive.x-unicode", "cursive");
user_pref("font.name.cursive.x-western", "cursive");

/* Monospace should be equal to variable */
user_pref("font.size.monospace.x-math", 16);
user_pref("font.size.monospace.x-unicode", 16);
user_pref("font.size.monospace.x-western", 16);
