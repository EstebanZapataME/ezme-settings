// ==UserScript==
// @name          Better text rendering?
// @description	  Legibility, and a very faint shadow
// @author        Esteban Zapata
// @homepage      https://gitlab.com/EstebanZapataME/ezme-settings/-/blob/master/multi/userscript/text_rendering.user.js
// @include       http://*
// @include       https://*
// @run-at        document-start
// @version       1.0
// ==/UserScript==
(function() {var css = [
	"*	{",
	"	text-rendering:	optimizeLegibility;",
	"	text-shadow:	0px 0px 0.75px;",
	"}"
].join("\n");
if (typeof GM_addStyle != "undefined") {
	GM_addStyle(css);
} else if (typeof PRO_addStyle != "undefined") {
	PRO_addStyle(css);
} else if (typeof addStyle != "undefined") {
	addStyle(css);
} else {
	var node = document.createElement("style");
	node.type = "text/css";
	node.appendChild(document.createTextNode(css));
	var heads = document.getElementsByTagName("head");
	if (heads.length > 0) {
		heads[0].appendChild(node);
	} else {
		// no head yet, stick it whereever
		document.documentElement.appendChild(node);
	}
}
})();

