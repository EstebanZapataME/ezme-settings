// ==UserScript==
// @name          Highlight AUR dependencies on AUR
// @namespace     http://userstyles.org
// @description	  Adds a small, red “AUR” label next to AUR dependencies of AUR packages.
// @author        Alexandre Macabies
// @homepage      https://userstyles.org/styles/146897
// @include       http://aur.archlinux.org/*
// @include       https://aur.archlinux.org/*
// @include       http://*.aur.archlinux.org/*
// @include       https://*.aur.archlinux.org/*
// @run-at        document-start
// @version       0.20170820124855
// ==/UserScript==
(function() {var css = [
	"#pkgdeps li > a[href^=\"/packages/\"]:after {",
	"  content: \'AUR\';",
	"  display: inline-block;",
	"  vertical-align: super;",
	"  font-size: 0.8em;",
	"  color: red;",
	"}"
].join("\n");
if (typeof GM_addStyle != "undefined") {
	GM_addStyle(css);
} else if (typeof PRO_addStyle != "undefined") {
	PRO_addStyle(css);
} else if (typeof addStyle != "undefined") {
	addStyle(css);
} else {
	var node = document.createElement("style");
	node.type = "text/css";
	node.appendChild(document.createTextNode(css));
	var heads = document.getElementsByTagName("head");
	if (heads.length > 0) {
		heads[0].appendChild(node);
	} else {
		// no head yet, stick it whereever
		document.documentElement.appendChild(node);
	}
}
})();
