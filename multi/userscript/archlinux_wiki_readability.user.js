// ==UserScript==
// @name          Arch Linux Wiki (readability)
// @namespace     http://userstyles.org
// @description	  Improved readability of Arch Linux Wiki. New header sizes to be better aware of the current section you are reading.
// @author        task
// @homepage      https://userstyles.org/styles/92488
// @include       http://wiki.archlinux.org/*
// @include       https://wiki.archlinux.org/*
// @include       http://*.wiki.archlinux.org/*
// @include       https://*.wiki.archlinux.org/*
// @run-at        document-start
// @version       0.20161218121127
// ==/UserScript==
(function() {var css = [
	"* { font-family: \"Linux Biolinum O\", Georgia, serif; font-size: 1.0rem;}",
	"    body, #content, table { background-color: #ddd; color: #222; }",
	"    h1 span{ margin-top: 40px; font-weight: normal; font-variant: small-caps; font-size: 2em; line-height: 130%; color: #2A5680; letter-spacing: -0.2pt; word-spacing: -0.2pt; border-bottom: 3px solid #667799;}",
	"    h2 span{ margin-top: 30px;  font-size: 1.9em; color: #445577; line-height: 140%; color: #2A5680; letter-spacing: -0.3pt; word-spacing: -0.2pt; font-weight: normal; }",
	"    h3 span{ margin-top: 20px; font-weight: normal; font-size: 1.6em; letter-spacing: -0.2pt; word-spacing: -0.1pt; color: #446688;}",
	"    h4 span{ margin-top: 10px;  font-size: 1.4em; font-weight: normal; letter-spacing: -0.2pt; word-spacing: -0.1pt; color: #556688;}",
	"    h5 span{ margin-top: 0px;  font-weight: normal; font-size: 1.2em; color: #667799;}",
	"    b { font-weight:500; color: #444444;}",
	"    div#content { width: 960px; background-color: #eeeeee; margin: 3.75em 0px 0px 13.2em;}",
	"    div#column-content { background-color: #ffffff; margin: -1px 0px 0.6em -17.2em;}",
	"    ul { text-align: left; padding: 3px;}",
	"    #toc, .toc, .mw-warning { color: #445577; background-color: #eeeeee; border: 0px solid #D7DFE3;}",
	"    #p-search, .pBody { padding: 0.5em 0.4em 0.4em; background-color: #ffffff; border: 0px solid #333333; font-size: 12px; width: 160px;}",
	"    #p-cactions { margin: 0px 0px 0px 27px;  }",
	"    pre {font-size: 85%; width: 930px;}"
].join("\n");
if (typeof GM_addStyle != "undefined") {
	GM_addStyle(css);
} else if (typeof PRO_addStyle != "undefined") {
	PRO_addStyle(css);
} else if (typeof addStyle != "undefined") {
	addStyle(css);
} else {
	var node = document.createElement("style");
	node.type = "text/css";
	node.appendChild(document.createTextNode(css));
	var heads = document.getElementsByTagName("head");
	if (heads.length > 0) {
		heads[0].appendChild(node);
	} else {
		// no head yet, stick it whereever
		document.documentElement.appendChild(node);
	}
}
})();
