// ==UserScript==
// @name          SCP Fonts
// @namespace     scp-wiki.wikidot.com
// @description	  Changes the Sigma9 fonts for the Blackboard ones.
// @author        Esteban Zapata
// @homepage      https://gitlab.com/EstebanZapataME/ezme-settings/-/blob/master/multi/firefox/chrome/userContent/scp_fonts.css
// @include       http://scp-wiki.wikidot.com/*
// @include       https://scp-wiki.wikidot.com/*
// @include       http://*.scp-wiki.wikidot.com/*
// @include       https://*.scp-wiki.wikidot.com/*
// @run-at        document-start
// @version       3
// ==/UserScript==
(function() {var css = [
	"@font-face { font-family: 'Inter var'; font-style: normal; font-weight: 100 900; src: url('https://rsms.me/inter/font-files/Inter-roman.var.woff2') format('woff2'); font-named-instance: 'Regular'; font-feature-settings: "lnum", "pnum", "kern", "cpsp", "liga", "case", "clig", "dlig" 0, "calt", "aalt" 0, "ss01", "ss02" 0, "ss03", "ss04", "zero", "cv11" 0; } @font-face { font-family: 'Inter var'; font-style: italic; font-weight: 100 900; src: url('https://rsms.me/inter/font-files/Inter-italic.var.woff2') format('woff2'); font-named-instance: 'Italic'; font-feature-settings: "lnum", "pnum", "kern", "cpsp", "liga", "case", "clig", "dlig" 0, "calt", "aalt" 0, "ss01", "ss02" 0, "ss03", "ss04", "zero", "cv11" 0; } @font-face { font-family: 'Inter'; font-style: normal; font-weight: 400; src: url("https://rsms.me/inter/font-files/Inter-Regular.woff2") format("woff2"); font-feature-settings: "lnum", "pnum", "kern", "cpsp", "liga", "case", "clig", "dlig" 0, "calt", "aalt" 0, "ss01", "ss02" 0, "ss03", "ss04", "zero", "cv11" 0; } @font-face { font-family: 'Inter'; font-style: normal; font-weight: 700; src: url("https://rsms.me/inter/font-files/Inter-Bold.woff2") format("woff2"); font-feature-settings: "lnum", "pnum", "kern", "cpsp", "liga", "case", "clig", "dlig" 0, "calt", "aalt" 0, "ss01", "ss02" 0, "ss03", "ss04", "zero", "cv11" 0; } body { font-family: 'Inter var', 'Inter', sans-serif !important; font-feature-settings: "lnum", "pnum", "kern", "cpsp", "liga", "case", "clig", "dlig" 0, "calt", "aalt" 0, "ss01", "ss02" 0, "ss03", "ss04", "zero", "cv11" 0; } @font-face { font-family: 'Recursive'; font-style: normal; font-weight: 100 900; src: url('https://nu-scptheme.github.io/Black-Highlighter/fonts/Recursive_VF_1.077.woff2') format('woff2'); } tt { font-family: 'Recursive', monospace !important; }"
].join("\n");
if (typeof GM_addStyle != "undefined") {
	GM_addStyle(css);
} else if (typeof PRO_addStyle != "undefined") {
	PRO_addStyle(css);
} else if (typeof addStyle != "undefined") {
	addStyle(css);
} else {
	var node = document.createElement("style");
	node.type = "text/css";
	node.appendChild(document.createTextNode(css));
	var heads = document.getElementsByTagName("head");
	if (heads.length > 0) {
		heads[0].appendChild(node);
	} else {
		// no head yet, stick it whereever
		document.documentElement.appendChild(node);
	}
}
})();
