// ==UserScript==
// @name          Voodoo Adbypas
// @namespace     mc.voodoobeard.com
// @match         https://mc.voodoobeard.com/
// @homepage      https://gitlab.com/EstebanZapataME/ezme-settings/-/blob/master/multi/userscript/Voodoo_Adbypas.user.js
// @updateURL     https://gitlab.com/EstebanZapataME/ezme-settings/-/raw/master/multi/userscript/Voodoo_Adbypas.user.js
// @downloadURL   https://gitlab.com/EstebanZapataME/ezme-settings/-/raw/master/multi/userscript/Voodoo_Adbypas.user.js
// @grant         none
// @version       1.0
// ==/UserScript==

var lookup = {
  "Bed Sleep Menu System (One Player Triggered)": "SleepMenu",
  "Mob Muffler": "MobMuffler",
  "Shulkermites": "Shulkermites",
  "Anti Ender-Grief": "Anti_Ender-Grief",
  "Shulkers Drop 2 Shells": "ShulkerShells",
  "Elytra Dragon Loot": "Ender_Dragon_Loot",
  "Gravestones": "Gravestones",
  "Item Magnet": "Item_Magnet",
  "Server Friendly Wither": "Server_Friendly_Wither",
  "Tridents O' Plenty": "Tridents_o_Plenty",
  "Anti Zombie Breach": "Anti-Zombie_Breach",
  "Possessive Pigmen": "Possessive_Pigmen",
  "Nether Creepers": "Nether_Creepers",
  "AFK Detection": "AFK_Detection",
  "Chicken Shedding": "Chicken_Shedding",
  "End Ship Beacons": "End_Ship_Beacons",
  "Crop Harvesting": "Crop_Harvesting",
  "Spawner Scanning": "Spawner_Scanning",
  "Death Book": "Death_Book",
  "Ender Beacons": "Ender_Beacons",
  "Paperbark": "Paperbark",
  "Creeper Dungeon": "Creeper_Dungeon",
  "Mendfinity": "Mendfinity",
  "Auto-Plant Saplings": "Auto-Plant_Saplings",
  "Plated Elytra": "Plated_Elytra",
  "Weakened Bedrock": "Weakened_Bedrock",
  "Mob Statues": "Mob_Statues_Vanilla",
  "Tree Pots": "Tree_Pots",
  "PvP Player Heads": "PvP_Player_Heads",
  "No More Zoombies": "No_More_Zoombies",
  "Disenchanting": "Disenchanting",
  "Deep Mobs": "Deep_Mobs",
  "Extra Dragon Eggs": "Extra_Dragon_Eggs",
  "Mobcutter": "Mobcutter",
  "Trader Notify": "Trader_Notify",
  "Killer Bunny": "Killer_Bunny",
  "Elevators": "Elevators",
  "Assembled Slabs": "Assembled_Slabs",
  "Apiarist Suit": "Apiarist_Suit",
  "Movable Cakes": "Movable_Cakes",
  "Withering Darkness": "Withering_Darkness",
  "Bat Membranes": "Bat_Membranes",
  "Starting Inventory": "Starting_Inventory",
  "Effective Netherite Armour": "Effective_Netherite_Armour",
  "Invisible Item Frames": "Invisible_Item_Frames",
  "Beehive Lore": "Beehive_Lore",
  "Shulker Respawning": "Shulker_Respawning",
  "Climbable Chains": "Climbable_Chains",
  "Silk Touch Amethyst": "Silk_Touch_Amethyst",
  "Copper Beacons": "Copper_Beacons",
  "Conductive Elytra": "Conductive_Elytra",
  "World Pregen": "World_Pregen",
  "Trim Trader": "Trim_Trader",
  "Mixed Stone Tools": "Mixed_Stone_Tools",
  "Zombie Leather": "Zombie_Leather",
  "Botanical Replication": "Botanical_Replication",
  "Equestrian Craftables": "Equestrian_Craftables",
  "Layers2Blocks": "Layers2Blocks",
  "Progressive Armour Crafting": "Progressive_Armour_Crafting",
  "Craftable Bell": "Craftable_Bell",
  "Blasted Redstone Blocks": "Blasted_Redstone_Blocks",
  "Bone Black": "Bone_Black",
  "Sticky Honey Pistons": "Sticky_Honey_Pistons",
  "Further Fermentation": "Further_Fermentation",
  "Blasted Ore Blocks": "Blasted_Ore_Blocks",
  "Crafty Oxidisation": "Crafty_Oxidation",
  "Wither Bats": "UHC_module_WitherBats",
  "CutClean": "UHC_module_CutClean",
}

for (let i of document.getElementsByClassName("card mb-4 box-shadow")) {
  let mod_version = i.outerText.split("\nv ")[1]?.match(/[\d(\.)]+/)[0];
  let mc_version = i.outerText.split("\nv ")[1]?.split("MC ")[1]?.trim();
  let e_name = i.childNodes[5].children[0].textContent;
  if (!lookup[e_name]) break;
  i.innerHTML = i.innerHTML.replace(/http[s]?:\/\/(dl.voodoobeard.com\/\w+|adfoc.us\/\d+)/g, `https://mc.voodoobeard.com/downloads/Datapacks/${mc_version}/${lookup[e_name]}_${mod_version}.zip`);
}