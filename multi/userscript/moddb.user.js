// ==UserScript==
// @name          mod db larger download
// @namespace     moddb.com
// @description	  This is to make the text larger.
// @author        EstebanZapataME
// @homepage      https://gitlab.com/EstebanZapataME/ezme-settings/-/blob/master/multi/userscript/moddb.user.js
// @updateURL     https://gitlab.com/EstebanZapataME/ezme-settings/-/raw/master/multi/userscript/moddb.user.js
// @downloadURL   https://gitlab.com/EstebanZapataME/ezme-settings/-/raw/master/multi/userscript/moddb.user.js
// @icon          https://www.moddb.com/favicon.png
// @include       https://*.moddb.com/downloads/*
// @include       https://moddb.com/downloads/*
// @run-at        document-start
// @version       1
// ==/UserScript==
(function() {var css = [
	"a, p {font-size: 32px !important;}span {font-size: 24px !important;}"
].join("\n");
if (typeof GM_addStyle != "undefined") {
	GM_addStyle(css);
} else if (typeof PRO_addStyle != "undefined") {
	PRO_addStyle(css);
} else if (typeof addStyle != "undefined") {
	addStyle(css);
} else {
	var node = document.createElement("style");
	node.type = "text/css";
	node.appendChild(document.createTextNode(css));
	var heads = document.getElementsByTagName("head");
	if (heads.length > 0) {
		heads[0].appendChild(node);
	} else {
		// no head yet, stick it whereever
		document.documentElement.appendChild(node);
	}
}
})();
