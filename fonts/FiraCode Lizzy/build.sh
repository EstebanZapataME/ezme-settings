#!/bin/bash

git clone --depth=1 https://github.com/tonsky/FiraCode.git

cd FiraCode

./script/build.sh --features "cv02,cv12,ss05,ss03,cv31,cv30,cv20,cv21,cv27,ss06" --family-name "FiraCode Lizzy" --weights "Regular"

